function getSize() {
    var w = window.innerWidth;
    var h = window.innerHeight;
    if(650 >= w) {
        adjustHeader();
    }
    adjustSlider(h);
}

function adjustHeader() {
    var w = window.innerWidth;
    var h = window.innerHeight;
    if(650 >= w) {
        var navbar = document.getElementById('navbar');
        navbar.className = "navbar navbar-default navbar-small";
        var logo = document.getElementById('logo-icon');
        var newLogo = new Image();
        logo.src = "http://rococoglobaltechnologies.com/wp-content/uploads/2014/09/Rococo-Icon-White-HD.png";
    } else {
        var navbar = document.getElementById('navbar');
        navbar.className = "navbar navbar-default navbar-fixed-top";
        var logo = document.getElementById('logo-icon');
        var newLogo = new Image();
        logo.src = "http://rococoglobaltechnologies.com/wp-content/uploads/2014/10/Rococo-Logo-HD.png";
    }
    adjustSlider(h)
}

function adjustSlider(windowHeight) {
//            console.log("Window height: " + windowHeight);
    var carouselInner = document.getElementById('carousel-inner');
    if(undefined != carouselInner) {
        if(500 < windowHeight - 400) {
            carouselInner.style.setProperty("max-height", (windowHeight - 400)  + "px", "important");
//                console.log("Inner height: " + carouselInner.style.maxHeight);
        } else {
            carouselInner.style.setProperty("max-height", "500px", "important");
//                console.log("Inner height: " + carouselInner.style.maxHeight);
        }
        var captions = document.getElementsByClassName('carousel-caption');
        var maxHeight = carouselInner.style.maxHeight.replace("px","");
        var bottomValue = 0;
        var flag = false;
        if(maxHeight > windowHeight) {
            bottomValue = maxHeight - windowHeight;
            flag = true;
        } else {
            bottomValue = windowHeight - maxHeight;
        }
        if(150 < bottomValue && flag) {
            bottomValue = 50;
        } else if(50 > bottomValue) {
            bottomValue = 50;
        } else if(70 < bottomValue && 120 > bottomValue) {
            bottomValue = 120;
        }

        for (i = 0; i < captions.length; i++) { 
            captions[i].style.setProperty("bottom", bottomValue + "px", "important");
        }
    }
}

window.onscroll = function (event) {
    var contactVideo = document.getElementById('contact-video');
    if(undefined != contactVideo) {
        if(contactVideo.scrollHeight/2 < getScrollXY()[1]) {
            var navbar = document.getElementById('navbar');
            if(navbar.className.indexOf("navbar-small") == -1) {
                navbar.className = "navbar navbar-default navbar-fixed-top with-background";
                var logo = document.getElementById('logo-icon');
                var newLogo = new Image();
                logo.src = "http://rococoglobaltechnologies.com/wp-content/uploads/2014/09/Rococo-Icon-White-HD.png";
            }
        } else {
            var navbar = document.getElementById('navbar');
            if(navbar.className.indexOf("navbar-small") == -1) {
                navbar.className = "navbar navbar-default navbar-fixed-top";
                var logo = document.getElementById('logo-icon');
                var newLogo = new Image();
                logo.src = "http://rococoglobaltechnologies.com/wp-content/uploads/2014/10/Rococo-Logo-HD.png";
            }
        }
    }
}

function getScrollXY() {
    var scrOfX = 0, scrOfY = 0;
    if( typeof( window.pageYOffset ) == 'number' ) {
    //Netscape compliant
        scrOfY = window.pageYOffset;
        scrOfX = window.pageXOffset;
    } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
    //DOM compliant
        scrOfY = document.body.scrollTop;
        scrOfX = document.body.scrollLeft;
    } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
    //IE6 standards compliant mode
        scrOfY = document.documentElement.scrollTop;
        scrOfX = document.documentElement.scrollLeft;
    }
    return [ scrOfX, scrOfY ];
} 